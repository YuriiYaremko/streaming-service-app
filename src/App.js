import React, { useEffect } from 'react';
import './App.css';
import HomeScreen from './screens/HomeScreen';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import LoginScreen from './screens/LoginScreen';
import db, { auth } from './firebase';
import { login, logout, selectUser } from './features/userSlice';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import ProfileScreen from './screens/ProfileScreen';
import LikedScreen from './screens/LikedScreen';
import FriendsScreen from './screens/FriendsScreen';
import UsersScreen from './screens/UsersScreen';


function App() {
  const user = useSelector(selectUser)
  const dispatch = useDispatch()

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged(userAuth => {
      if (userAuth) {
        //Logged in
        dispatch(login({
          uid: userAuth.uid,
          email: userAuth.email
        }))
        const userFromFirestore = db.collection('users').doc(userAuth.uid)

        userFromFirestore.get()
          .then((docSnapshot) => {
            if (!docSnapshot.exists) {
              userFromFirestore.set({
                username: userAuth.email.split('@')[0],
                email: userAuth.email,
                favorites: [],
                friends: []
              }).then(() => console.log('Success'))
            }
          })
      } else {
        //Logged out
        dispatch(logout())
      }
    })

    return unsubscribe
  }, [dispatch])

  return (
    <div className="app">
      <Router>
        {!user ? (
          <LoginScreen />
        ) : (
          <Switch>
            <Route exact path="/profile">
              <ProfileScreen />
            </Route>
            <Route exact path="/">
              <HomeScreen />
            </Route>
            <Route exact path="/liked">
              <LikedScreen />
            </Route>
            <Route exact path="/friends">
              <FriendsScreen />
            </Route>
            <Route exact path="/users">
              <UsersScreen />
            </Route>
          </Switch>
        )}

      </Router>
    </div>
  );
}

export default App;
