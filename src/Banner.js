import './Banner.css'

function Banner() {

    return (
        <header className="banner" style={{
            backgroundSize: "cover",
            backgroundImage: `url("https://coolbackgrounds.io/images/backgrounds/index/compute-ea4c57a4.png")`,
            backgroundPosition: "center center"
        }}>
            <div className="banner__titles">
                <h1>OBSERVE THE WORLD OF THE MOVIES</h1>
                <h3>You Will Definitely Find Something For You</h3>
            </div>
        </header >
    )
}

export default Banner