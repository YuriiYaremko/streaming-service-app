import React from 'react'
import './Footer.css'

function Footer() {
    return (
        <div className="footer" style={{
            backgroundSize: "cover",
            backgroundImage: `url("https://coolbackgrounds.io/images/backgrounds/index/compute-ea4c57a4.png")`,
            backgroundPosition: "center center"
        }}>
            <h3>Thank you for choosing</h3>
            <h1><span>M</span>ovie<span> W</span>orld</h1>
            <h3>We appreciate that</h3>
        </div>
    )
}

export default Footer
