import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router'
import './Nav.css'

function Nav() {

    const [show, handleShow] = useState(false)
    const history = useHistory()

    const transitionNavBar = () => {
        if (window.scrollY > 100) {
            handleShow(true)
        } else {
            handleShow(false)
        }
    }

    useEffect(() => {
        window.addEventListener('scroll', transitionNavBar)
        return () => window.removeEventListener('scroll', transitionNavBar)

    }, [])

    return (
        <div className={`nav ${show && 'nav__black'}`}>
            <div className="nav__contents">
                <h1 onClick={() => history.push("/")} className="nav__logo">MW</h1>

                <h2 onClick={() => history.push("/liked")}>Liked Movies</h2>
                <h2 onClick={() => history.push("/friends")}>Friends</h2>
                <h2 onClick={() => history.push("/users")}>Users</h2>
                <img
                    onClick={() => history.push("/profile")} className="nav__avatar" src="https://cdn-icons-png.flaticon.com/512/149/149071.png" alt="" />
            </div>

        </div>
    )
}

export default Nav

