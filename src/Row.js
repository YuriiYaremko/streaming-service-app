import React, { useEffect, useState } from "react";
import "./Row.css";
import axios from "./axios";
import { useDispatch, useSelector } from "react-redux";
import { liked } from "./features/likedSlice";
import { selectUser } from "./features/userSlice";
import db from "./firebase";
import { doc, getDoc, updateDoc, arrayUnion } from "firebase/firestore";



function Row({ genres }) {
    const [movies, setMovies] = useState([]);
    const [sortedMovies, setIsSortedMovies] = useState([]);
    const [isSorted, setIsSorted] = useState(false);
    const [likedMov, setLikedMov] = useState([])

    const user = useSelector(selectUser)

    const dispatch = useDispatch()

    const userFirestore = doc(db, "users", user.uid);

    useEffect(() => {
        async function fetchData() {
            const request = await axios.get();
            setMovies(request.data.filter((film) => film.genres.includes(genres)));
            setIsSortedMovies(request.data.sort((a, b) => {
                return b.rating.average - a.rating.average;
            }))

            const docRef = doc(db, "users", user.uid);
            const docSnap = await getDoc(docRef);
            if (docSnap.data()?.favorites.length > 0) {
                setLikedMov(docSnap.data().favorites)
            }
            return request;
        }
        fetchData();
    }, []);

    const sortMovies = () => {
        setIsSorted(!isSorted);
    };

    const truncate = (str, n) => {
        return str?.length > n ? str.substr(0, n - 1) + "..." : str;
    }

    const renderMovies = isSorted ? sortedMovies : movies;

    return (
        <div className="row">
            <h2>{genres}</h2>
            <button onClick={(e) => sortMovies()} className="row__button">
                &#8678; Sort By Rating &#8680;
            </button>

            <div className="row__posters">
                {renderMovies.map((movie) => (
                    <div className="row__posterContainer" key={movie.id}>
                        <img
                            className={"row__poster row__posterLarge"}
                            src={movie.image.medium}
                            alt={movie.name}
                        />
                        <ul className="row__listDescription">
                            <li><h3>{movie.name}</h3></li>
                            <li><strong>Rating: </strong> {movie.rating.average}</li>
                            <li><strong>Genres: </strong>{movie.genres.map((genre, id) => {
                                return id === movie.genres.length - 1 ? genre : `${genre}/`
                            })}</li>
                            <li><strong>Summary: </strong> {<br />}{truncate(movie.summary.replace(/<[^>]*>?/gm, ''), 100)}</li>
                        </ul>

                        {likedMov.filter(el => movie.id === el.id).length === 0 ? (<button onClick={(e) => (
                            e.currentTarget.disabled,
                            e.currentTarget.style.backgroundColor = 'goldenrod',
                            dispatch(liked(movie)),
                            updateDoc(userFirestore, {
                                favorites: arrayUnion(movie)
                            })
                        )} className="row__button">Like</button>) : (
                            <button disabled className="row__button row__liked">Liked</button>
                        )}
                    </div>
                ))}
            </div>
        </div >
    );
}

export default Row;