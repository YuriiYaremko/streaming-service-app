import { configureStore } from '@reduxjs/toolkit';
import userReducer from '../features/userSlice';
import likedReducer from '../features/likedSlice'

export const store = configureStore({
  reducer: {
    user: userReducer,
    likedMovies: likedReducer
  },
});
