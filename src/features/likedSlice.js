import { createSlice } from '@reduxjs/toolkit';

export const likedSlice = createSlice({
    name: 'liked',
    initialState: {
        liked: []
    },
    reducers: {
        liked: (state, action) => {
            state.liked = [...state.liked, action.payload]
        },
        dislike: (state, action) => {
            state.liked = [...action.payload]
        }

    }
});

export const { liked, dislike } = likedSlice.actions;

export const likedMovies = (state) => state.likedMovies.liked;

export default likedSlice.reducer;