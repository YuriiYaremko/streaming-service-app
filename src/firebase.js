import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';


const firebaseConfig = {
    apiKey: "AIzaSyAk8fMLPcS9o8wjoHAYk44_cPS15C2xjQI",
    authDomain: "streaming-service-2f964.firebaseapp.com",
    projectId: "streaming-service-2f964",
    storageBucket: "streaming-service-2f964.appspot.com",
    messagingSenderId: "159615543849",
    appId: "1:159615543849:web:869e3fcf4fcf90fff9ca38"
};

const firebaseApp = firebase.initializeApp(firebaseConfig)
const db = firebaseApp.firestore()
db.settings({ timestampsInSnapshots: true })
const auth = firebase.auth()

export { auth }
export default db
