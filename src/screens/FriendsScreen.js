import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import Banner from '../Banner'
import db from '../firebase'
import Footer from '../Footer'
import Nav from '../Nav'
import './FriendsScreen.css'
import { doc, getDoc } from "firebase/firestore";
import { selectUser } from '../features/userSlice'
import { useHistory } from 'react-router'


function FriendsScreen() {
    const [friends, setFriends] = useState([])
    const user = useSelector(selectUser)
    const history = useHistory()

    useEffect(() => {
        const getCollection = async (collectionPath) => {
            const docRef = doc(db, "users", user.uid);
            const docSnap = await getDoc(docRef);
            if (docSnap.data().friends.length > 0) {
                setFriends(docSnap.data().friends)
            }

            return docSnap;
        }
        getCollection('users')
    }, [])


    console.log(friends);

    const truncate = (str, n) => {
        return str?.length > n ? str.substr(0, n - 1) + "..." : str;
    }

    return (
        <div>
            <Nav />
            <Banner />
            {friends.length === 0 ? (
                <div className="friends friends__empty">
                    <h1>Press the button to make friends</h1>
                    <button className="friends__makeFriends" onClick={() => history.push("/users")}>Make Friends</button>
                </div>
            ) : (
                <div >

                    {friends.map(el => (
                        <div className="friends friends__posters" key={el.id}>
                            <h2>{el.username}</h2>
                            {el.favorites.length === 0 ? (<h1 className="friends__emptyFriendList">User Don't Have Liked Films</h1>)
                                : (el.favorites.map(movie => (
                                    <div className="friends__posterContainer" key={movie.id}>
                                        <img
                                            className={"friends__poster friends__posterLarge"}
                                            src={movie.image.medium}
                                            alt={movie.name}
                                        />
                                        <ul className="friends__listDescription">
                                            <li><h3>{movie.name}</h3></li>
                                            <li><strong>Rating: </strong> {movie.rating.average}</li>
                                            <li><strong>Genres: </strong>{movie.genres.map((genre, id) => {
                                                return id === movie.genres.length - 1 ? genre : `${genre}/`
                                            })}</li>
                                            <li><strong>Summary: </strong> {<br />}{truncate(movie.summary.replace(/<[^>]*>?/gm, ''), 100)}</li>
                                        </ul>
                                    </div>
                                )))
                            }</div>
                    ))}
                </div>
            )}

            <Footer />
        </div>
    )
}

export default FriendsScreen
