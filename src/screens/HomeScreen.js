import React from "react";
import Banner from "../Banner";
import './HomeScreen.css'
import Nav from "../Nav";
import Row from "../Row";
import Footer from "../Footer";

function HomeScreen() {
    return (
        <div className="homeScreen">
            <Nav />
            <Banner />
            <Row genres="Science-Fiction" />
            <Row genres="Thriller" />
            <Row genres="Crime" />
            <Row genres="Action" />
            <Row genres="Supernatural" />
            <Row genres="Horror" />
            <Row genres="Comedy" />
            <Footer />
        </div>
    )
}

export default HomeScreen
