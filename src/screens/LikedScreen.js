import React, { useEffect, useState } from 'react'
import Banner from '../Banner'
import Footer from '../Footer'
import Nav from '../Nav'
import { useDispatch, useSelector } from 'react-redux'
import { dislike } from '../features/likedSlice'
import { useHistory } from 'react-router'
import './LikedScreen.css'
import db from '../firebase'
import { doc, getDoc, updateDoc, arrayRemove } from "firebase/firestore";
import { selectUser } from '../features/userSlice'


function LikedScreen() {
    const [liked, setLiked] = useState([])
    const user = useSelector(selectUser)
    const userFirestore = doc(db, "users", user.uid);

    useEffect(() => {
        async function fetchData() {
            const docRef = doc(db, "users", user.uid);
            const docSnap = await getDoc(docRef);
            setLiked(docSnap.data().favorites)
            return docSnap.data()
        }
        fetchData()
    }, [])

    const dispatch = useDispatch()
    const history = useHistory()


    const truncate = (str, n) => {
        return str?.length > n ? str.substr(0, n - 1) + "..." : str;
    }

    return (
        <div>
            <Nav />
            <Banner />
            <div className="liked liked__posters">
                {liked.length > 0 ? (liked.map((movie) => (
                    <div className="liked__posterContainer" key={movie.id}>
                        <img
                            className={"liked__poster liked__posterLarge"}
                            src={movie.image.medium}
                            alt={movie.name}
                        />
                        <ul className="liked__listDescription">
                            <li><h3>{movie.name}</h3></li>
                            <li><strong>Rating: </strong> {movie.rating.average}</li>
                            <li><strong>Genres: </strong>{movie.genres.map((genre, id) => {
                                return id === movie.genres.length - 1 ? genre : `${genre}/`
                            })}</li>
                            <li><strong>Summary: </strong> {<br />}{truncate(movie.summary.replace(/<[^>]*>?/gm, ''), 100)}</li>
                        </ul>
                        <button onClick={() => {
                            dispatch(dislike(liked.filter(el => el.id !== movie.id)))
                            setLiked(liked.filter(item => item.id !== movie.id))
                            updateDoc(userFirestore, {
                                favorites: arrayRemove(movie)
                            })
                        }} className="liked__button liked__buttonDislike">Dislike</button>
                    </div>
                ))) : <div className="liked__emptyFilms">
                    <h1>You haven't chosen anything, back to the main page</h1>
                    <button className='liked__button' onClick={() => history.push("/")}>Watch The Movies</button>
                </div>}

            </div>
            <Footer />
        </div>
    )
}

export default LikedScreen
