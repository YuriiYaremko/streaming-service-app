import React, { useState } from 'react'
import './LoginScreen.css'
import SignupScreen from './SignupScreen'

function LoginScreen() {
    const [signIn, setSignIn] = useState(false)

    return (
        <div className='loginScreen'>
            <div className="loginScreen__background">
                <h1 className="loginScreen__logo">MW</h1>

                <button onClick={() => setSignIn(true)} className="loginScreen__button">Sign In</button>
                <div className="loginScreen__gradient"></div>
                <div className="loginScreen__body">
                    {signIn ? (
                        <SignupScreen />
                    ) : (
                        <>
                            <h1>HELLO, WE ARE HAPPY TO SEE YOU HERE</h1>
                            <h2>It's a best place where you can find your favorite movie</h2>
                            <h3>Press the button and let's start</h3>
                            <div className="loginScreen__input">
                                <form>
                                    <button onClick={() => setSignIn(true)} className="loginScreen__getStarted">GET STARTED</button>
                                </form>
                            </div>
                        </>
                    )}

                </div>
            </div>
        </div>
    )
}

export default LoginScreen
