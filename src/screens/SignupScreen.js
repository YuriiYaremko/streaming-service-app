import React, { useRef, useState } from 'react'
import { useHistory } from 'react-router'
import { auth } from '../firebase'
import './SignupScreen.css'


function SignupScreen() {
    const emailRef = useRef(null)
    const passwordRef = useRef(null)
    const history = useHistory()
    const [reg, setReg] = useState(false)
    const title = reg ? 'Sign Up' : 'Sign In'

    const register = (e) => {
        e.preventDefault()

        auth.createUserWithEmailAndPassword(
            emailRef.current.value,
            passwordRef.current.value
        ).then(authUser => {
            console.log(authUser);
        }).catch(error => {
            alert(error.message)
        })

    }

    const signIn = (e) => {
        e.preventDefault()

        auth.signInWithEmailAndPassword(
            emailRef.current.value,
            passwordRef.current.value
        ).then(authUser => {
            console.log(authUser);
        }).catch(error => {
            alert(error.message)
        })
    }

    return (
        < div className="signupScreen" >
            <form>
                <h1>{title}</h1>
                <input ref={emailRef} placeholder="Email" type="email" />
                <input ref={passwordRef} placeholder="Password" type="password" />
                <button type="submit" onClick={(e) => {
                    reg ? register(e) : signIn(e)
                    history.push('/')
                }}>{title}</button>
                {!reg && (
                    <h4><span className="signupScreen__gray">New to MW? </span>
                        <span className="signupScreen__link" onClick={() => setReg(true)}>Sign Up now.</span></h4>
                )}
            </form>
        </div >
    )
}

export default SignupScreen
