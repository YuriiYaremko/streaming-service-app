import React, { useEffect, useState } from 'react'
import Banner from '../Banner'
import db from '../firebase'
import Footer from '../Footer'
import Nav from '../Nav'
import './UsersScreen.css'
import { doc, getDoc, updateDoc, arrayUnion } from "firebase/firestore";
import { useSelector } from 'react-redux'
import { selectUser } from '../features/userSlice'


function UsersScreen() {
    const [usersInfo, setUsersInfo] = useState([])
    const [friends, setFriends] = useState([])
    const user = useSelector(selectUser)

    const userFirestore = doc(db, "users", user.uid);



    useEffect(() => {
        const getCollection = async (collectionPath) => {
            const arr = [];
            const snapshot = await db.collection(collectionPath).get();

            snapshot.forEach((doc) => {
                arr.push({ ...doc.data(), id: doc.id });
            })
            setUsersInfo(arr)

            const docRef = doc(db, "users", user.uid);
            const docSnap = await getDoc(docRef);
            if (docSnap.data().friends.length > 0) {
                setFriends(docSnap.data().friends)
            }
            return arr;
        }
        getCollection('users')
    }, [])

    console.log(usersInfo);

    return (
        <div className="users">
            <Nav />
            <Banner />
            {usersInfo.length === 0 ? (
                <div className="users__empty">
                    <div class="lds-dual-ring"></div>
                </div>
            ) : (
                <ul className="users__list">
                    {usersInfo.filter(us => us.id !== user.uid).map(user => (
                        <li className="users__item" key={user.id}><span className="users__name">{user.username}</span>
                            <span className="users__email">{user.email}</span>
                            {friends.filter(item => item.id === user.id).length === 0 ? (
                                <button onClick={(e) => (
                                    e.currentTarget.disabled,
                                    e.currentTarget.innerHTML = 'Friend',
                                    e.currentTarget.style.backgroundColor = 'goldenrod',
                                    console.log(e.currentTarget),
                                    updateDoc(userFirestore, {
                                        friends: arrayUnion(user)
                                    })
                                )}
                                    className="users__buttonFriends">Make friends</button>
                            ) : (
                                <button disabled className="users__buttonFriends users__friendChosen">Friend</button>
                            )}
                        </li>
                    ))}
                </ul>
            )
            }
            <Footer />
        </div >
    )
}

export default UsersScreen
